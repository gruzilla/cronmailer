<?php

require __DIR__ . '/vendor/autoload.php';

// configuration

$username       = 'matthias.steinboeck@gmail.com';
$password       = 'xbxccswtgfxfnmov';
$url            = 'http://www.oeticket.com/fiva-wien-Tickets.html?doc=artistPages%2Ftickets&key=1436091%245887128';
$url            = 'http://www.oeticket.com/seiler-und-speer-weihnachtsshow-wien-Tickets.html?doc=artistPages%2Ftickets&action=tickets&key=1412650%246317959';
$pattern        = '#class=.unavailable#';
$pattern        = '#Eintrittskarte.*class=.unavailable#i';
$subject        = 'Fiva Event Notification';
$subject        = 'SeilerSpeer Event Notification';
$matchOrNoMatch = 0;
$sleep          = 5 * 60; // seconds


// initialization

$transporter = Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')
  ->setUsername($username)
  ->setPassword($password);

$mailer = Swift_Mailer::newInstance($transporter);




// business logic

while(true) {

    echo 'checking ... ';

    $data = file_get_contents($url);

    if ($matchOrNoMatch === preg_match($pattern, $data)) {
        echo 'pattern matching returned ' . $matchOrNoMatch . ', sending mail';
        $message = Swift_Message::newInstance($subject)
            ->setFrom(array('matthias.steinboeck@gmail.com' => 'Matthias Steinböck'))
            ->setTo(array('grillen@abendstille.at'=> 'Matthias Steinböck'))
            ->setBody(
                'pattern ' . $pattern . ' results with ' . $matchOrNoMatch .
                ' url checked: ' . $url
            )
        ;
        $mailer->send($message);
    }

    echo PHP_EOL;

    sleep($sleep);
}
